require('./Config/config.js')
const express= require('express');
const app=express();
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())

app.get('/', function(req, resp){
    resp.send("get")
})

app.get('/usuario', function(req, resp){
    resp.send("get")
})
app.post('/usuario', function(req, resp){
    let DatosUsuario=req.body;
    if(DatosUsuario.nombre===undefined)
    {
        resp.json({
            error:400,
            descripcion:"Falta ingresar el nomber"
        })
    }
    else{
        
        resp.send(DatosUsuario)
        }
})
app.put('/usuario/:id', function(req, resp){
    let idusuario=req.params.id;
    resp.json({
        idusuario
    });
    resp.send(idusuario);

})
app.delete('/usuario', function(req, resp){
    resp.send("delete");
    
})


app.listen (process.env.PORT, function(){
    console.log(`Escuchando en el puertoooo ${process.env.PORT}`)
})
